<?php
namespace DecideNow\SceneCrud\ORM\Traits;

trait HasPriorityTrait
{
	public static function priority_filter($query, $data = [])
	{
		//
	}
	
	public static function priorityNext($data = [])
	{
		$prior = self::select('priority');
		$prior = self::priority_filter($prior, $data);
		$prior = $prior->max('priority');
		$prior = ($prior !== null) ? $prior + 1 : 1;
		return $prior;
	}
	
	public function priorityUp($data = [])
	{
		$neighbour = self::where('priority', '>', $this->priority)->orderBy('priority', 'asc');
		$neighbour = self::priority_filter($neighbour, $data);
		$neighbour = $neighbour->first();
		if ($neighbour) {
			$tmp = $neighbour->priority;
			$neighbour->priority = $this->priority;
			$this->priority = $tmp;
			$neighbour->save();
			$this->save();
		}
		
	}
	
	public function priorityDn($data = [])
	{
		$neighbour = self::where('priority', '<', $this->priority)->orderBy('priority', 'desc');
		$neighbour = self::priority_filter($neighbour, $data);
		$neighbour = $neighbour->first();
		if ($neighbour) {
			$tmp = $neighbour->priority;
			$neighbour->priority = $this->priority;
			$this->priority = $tmp;
			$neighbour->save();
			$this->save();
		}
	}
	
	public static function priorityArange($data = [], $reverse = false)
	{
		$list = self::orderBy('priority', 'asc');
		$list = self::priority_filter($list, $data);
		$list = $list->get();
		$prior = ($reverse) ? $list->count() : 1;
		$prior_dir = ($reverse) ? -1 : 1;
		foreach($list as $list_item) {
			$list_item->priority = $prior;
			$list_item->save();
			$prior = $prior + $prior_dir;
		}
	}
	
}
