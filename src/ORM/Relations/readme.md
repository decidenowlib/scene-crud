# Usage

If there are many Computers in every point of sales. Point of sales is located on the Warehouse (shop) which belongs to Company.

### Define relation


```
class Computer extends Model
{
    use RelationChainable;
    ...
    /*
        returns Relation instance
    */
    public function company()
	{
		return $this->hasRelationChain(
			Company::class, // need to get company instance
			[
				'comp_company_t1' => ['table' => 'warehouse', 'right_key' => 'company_id'], // warehouse belongs to company
				'comp_company_t2' => ['table' => 'pos', 'right_key' => 'warehouse_id'], // pos is located on warehouse
				// can add as many tables as we need
				'comp_company_t3' => ['right_key' => 'pos_id'], // computer is located at poin of sales
				// at last step no need of table name, we have come to $this table
			],
			true // select single record, no need of first() method to access it
		);
	}
    ...
}
```
### Select data
```
$list = Computer::whereHas('company', function ($query) {
    $query->where('company.id', '=', Auth::user()->company_id);
});
```
or
```
Computer::first($id)->company->name;
```
