<?php

namespace DecideNow\SceneCrud;

use Illuminate\Support\ServiceProvider;
use DecideNow\SceneCrud\Commands\SceneCrudCreate;

class SceneCrudServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
		//
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		$this->loadViewsFrom(__DIR__.'/Commands/templates', 'command_templates');
		$this->loadViewsFrom(__DIR__.'/Views/vendor', 'scene-crud-views');
		$this->commands([
			SceneCrudCreate::class,
		]);
    }
}
