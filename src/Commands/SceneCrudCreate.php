<?php

namespace DecideNow\SceneCrud\Commands;

use DecideNow\Scene\Commands\Constructors\SceneConstructor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;

class SceneCrudCreate extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'scene-crud:create {scene_key} {--force}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create scene using template';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{	
		$scene_key = $this->argument('scene_key');
		$force = $this->option('force');
		
		$scene = new SceneConstructor($scene_key, $force);
		$scene->create();

		URL::forceRootUrl(config('app.url'));
		$this->info('');
		$this->info('Scene created:');
		$this->info(url($scene->getRouteData()['url']));
		$this->info('');
	}
	
}
