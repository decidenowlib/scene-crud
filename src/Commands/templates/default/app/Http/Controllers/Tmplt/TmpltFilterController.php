<?php
namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;

class ProductFilterController extends ProductController
{	
	public static $scene_key = 'product-filter';
	
	public $filter;
	
	public function prepareContent($request, $has_content = true)
	{
		$this->filter = $this->model->filterGetFromSession();

		$this->has_content = $has_content;
		return true;
	}

	public function prepareResponse($request, $has_content = true)
	{
		if ($has_content) {
			$this->prepareContent($request, $has_content);
		}
		return $this->sceneResponse();
	}
	
	public function get(Request $request)
	{
		$this->prepareGet($request);
		return $this->prepareResponse($request);
	}
	
	public function post(Request $request)
	{
		$this->preparePost($request);
		
		if ($this->task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		}
		return $this->prgRedirect() ?: $this->prepareResponse($request);
	}	
}