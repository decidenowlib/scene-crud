<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class Product extends ORMModel
{
	protected $table = 'product';
	protected $fillable = [
		'name', 'code', 'article', 'ean', 'description', 
	];
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Товары', 
			'record_name' => 'Товар', 
			'code' => 'Код', 
			'article' => 'Артикул', 
			'ean' => 'Штрих-код', 
			'description' => 'Описание',
			
			'filter_universal' => 'Отбор',

		] + parent::getAliases();
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'universal' => '', ];
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'universal', '');
		if ($tmp != '') {
			$query = $query
			->orWhere('name', 'LIKE', '%'.$tmp.'%')
			->orWhere('code', 'LIKE', '%'.$tmp.'%')
			->orWhere('article', 'LIKE', '%'.$tmp.'%')
			->orWhere('ean', 'LIKE', '%'.$tmp.'%')
			->orWhere('description', 'LIKE', '%'.$tmp.'%');
		}
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'code' => '', 'article' => '', 'ean' => '', 'description' => '', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'code', '');
		if ($tmp != '') {
			$query = $query->orderBy('code', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'article', '');
		if ($tmp != '') {
			$query = $query->orderBy('article', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'ean', '');
		if ($tmp != '') {
			$query = $query->orderBy('ean', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'description', '');
		if ($tmp != '') {
			$query = $query->orderBy('description', $tmp);
		}

		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	

	
	/* list queries */
	
	public static function defaultListQuery($query = null)
	{
		$query = parent::defaultListQuery($query);
		//$query = $query->select('product.*')->distinct();
		return $query;
	}

	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					//return true;
					return false;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
