@include('scene-message::alert')
@include('scene::content', $scene->childGet('product-item-scene')->sceneVariables())
@include('scene::content', $scene->childGet('product-filter-scene')->sceneVariables())
@include('scene::content', $scene->childGet('product-select-scene')->sceneVariables())

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-8">
			<h3>{{ $model::getAlias('table_name') }}</h3>
		</div>
		<div class="col-sm-4 text-right">
			<div class="form-group">
				@if ($multiple_mode)
					{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->open() !!}
						{!! FastDevCtrl::button('btn-multiple-check')->title('Пометить')->iconBefore('far fa-check-square fa-fw')->out() !!}
						{!! FastDevCtrl::button('btn-multiple-uncheck')->title('Снять пометку')->iconBefore('far fa-square fa-fw')->out() !!}
						<button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span class="caret"></span></button>
						<ul class="dropdown-menu pull-right">
							<li><a href="#" id="btn_multiple_delete" data-action-tech="{{ $scene->getActionTechnology('') }}">Удалить</a></li>
						</ul>
					{!! FastDevCtrl::buttonGroup('taskbar')->close() !!}
				@endif
				{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->open() !!}
					<div class="btn-group">
						<button class="btn btn-sm btn-default dropdown-toggle" type="button" data-toggle="dropdown">
							<i class="fas fa-stream"></i>&nbsp;<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							<li class="text-center"><a href="#" id="btn_per_page_15" data-count="15" data-action-tech="{{ $scene->getActionTechnology('') }}">15</a></li>
							<li class="text-center"><a href="#" id="btn_per_page_30" data-count="30" data-action-tech="{{ $scene->getActionTechnology('') }}">30</a></li>
							<li class="text-center"><a href="#" id="btn_per_page_40" data-count="45" data-action-tech="{{ $scene->getActionTechnology('') }}">45</a></li>
						</ul>
					</div>
					@if ($model->getPermission('create'))
						{!! FastDevCtrl::button('btn-create-item')->title('Создать')->iconBefore('glyphicon glyphicon-plus')->action('list_btn_create_item', $scene)->out() !!}
					@endif
					{!! FastDevCtrl::button('btn-show-filter')->title('Фильтр')->iconBefore('glyphicon glyphicon-filter')->style( (FastDevHlpr::array_is_empty($filter)) ? '' : 'success' )->out() !!}
					{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->action('list_btn_refresh', $scene)->out() !!}
					{!! FastDevCtrl::button('btn-multiple-mode-toggle')->title('Групповые операции')->iconBefore('fas fa-tasks fa-fw')->extClass($multiple_mode ? 'active' : '')->action('list_multiple_mode_toggle', $scene)->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->close() !!}
			</div>
		</div>
	</div>
	
	<div class="row list-hdr">
		<div class="col-sm-2 visible-xs">Сортировка: </div>
		@if ($model->getPermission('list', 'id'))
			<div class="col-sm-1">{!! FastDevCtrl::sortableLink('id', $model, $ordering)->action('list_sortable_header', $scene)->out() !!}</div>
		@endif
		<div class="col-sm-{{ ($model->getPermission('list', 'id')) ? '1' : '2' }}">{!! FastDevCtrl::sortableLink('name', $model, $ordering)->action('list_sortable_header')->out() !!}</div>
		<div class="col-sm-1">{!! FastDevCtrl::sortableLink('code', $model, $ordering)->action('list_sortable_header', $scene)->out() !!}</div>
		<div class="col-sm-1">{!! FastDevCtrl::sortableLink('article', $model, $ordering)->action('list_sortable_header', $scene)->out() !!}</div>
		<div class="col-sm-1">{!! FastDevCtrl::sortableLink('ean', $model, $ordering)->action('list_sortable_header', $scene)->out() !!}</div>
		<div class="col-sm-1">{!! FastDevCtrl::sortableLink('description', $model, $ordering)->action('list_sortable_header', $scene)->out() !!}</div>

		<div class="col-sm-2 hidden-xs">&nbsp;</div>
	</div>
	
	@foreach($list as $item)
	<div class="row list-item bottom-border" data-role="list-item" data-item-id = "{{ $item->id }}" data-item-text="{{ $item->toString() }}">
		@if ($model->getPermission('list', 'id'))
			<div class="col-sm-1">{{ $item->id }}</div>
		@endif
		<div class="col-sm-{{ ($model->getPermission('list', 'id')) ? '1' : '2' }}">
			@if($item->getPermission('edit')) <a id="link_edit_item" href="" data-action-tech="{{ $scene->getActionTechnology('list_link_edit_item') }}"> @endif
				{{ $item->toString() }}
			@if($item->getPermission('edit')) </a> @endif
		</div>
		<div class="col-sm-1">{{ $item->code }}</div>
		<div class="col-sm-1">{{ $item->article }}</div>
		<div class="col-sm-1">{{ $item->ean }}</div>
		<div class="col-sm-1">{{ $item->description }}</div>

		<div class="col-sm-2 text-right">
			{!! FastDevCtrl::buttonGroup('row-actions')->size('sm')->open() !!}
				@if ($item->getPermission('edit')) 
					{!! FastDevCtrl::button('btn-edit-item')->iconBefore('glyphicon glyphicon-pencil')->title('Редактировать')->action('list_btn_edit_item', $scene)->out() !!}
				@endif
				@if ($item->getPermission('delete')) 
					{!! FastDevCtrl::button('btn-delete-item')->iconBefore('glyphicon glyphicon-trash')->title('Удалить')->action('list_btn_delete_item', $scene)->out() !!}
				@endif
				@if ($multiple_mode)
					{!! FastDevCtrl::button('btn-check-item')->iconBefore('far fa-square fa-fw')->title('Пометить')->data('is_checked', '0')->out() !!}
				@endif
			{!! FastDevCtrl::buttonGroup('row-actions')->close() !!}
		</div>
	</div>
	@endforeach
	
	<div class="row">
		<div class="col-sm-12">
			{{ $list->links('scene-views::pagination.default', ['scene' => $scene]) }}
		</div>
	</div>
	
</div></div>

<form data-role="data_form">
	@include('scene::state')
</form>

{!! FastDevCtrl::textField('select-field')
    ->label('Текстовое поле')
    ->value('1')
    ->visibleValue('Значение 1')
	->hasSelectButton($scene->childGet('product-select-scene')->sceneId(), [
		'select-scene-size' => 'md', 'select-scene-position' => 'right'
	])
    ->hasClearButton()
    ->out() 
!!}
		