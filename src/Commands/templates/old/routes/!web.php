// Tmplt

Route::get('/tmp-lt', 'Tmplt\TmpltListController@get');
Route::post('/tmp-lt', 'Tmplt\TmpltListController@post');
Route::get('/tmp-lt/item/{id?}', 'Tmplt\TmpltItemController@get');
Route::post('/tmp-lt/item/{id?}', 'Tmplt\TmpltItemController@post');
Route::get('/tmp-lt/select', 'Tmplt\TmpltSelectController@get');
Route::post('/tmp-lt/select', 'Tmplt\TmpltSelectController@post');
Route::get('/tmp-lt/filter', 'Tmplt\TmpltFilterController@get');
Route::post('/tmp-lt/filter', 'Tmplt\TmpltFilterController@post');
