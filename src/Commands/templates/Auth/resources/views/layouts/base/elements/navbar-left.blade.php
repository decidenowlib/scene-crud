<ul class="navbar-nav mr-auto">
	@if (!Auth::guest() && Auth::user()->hasVerifiedEmail())
		<li class="nav-item">
			<a class="nav-link" href="{{ url('/user') }}">{{ App\ORM\User::getAlias('table_name') }}</a>
		</li>
	@else
	@endguest
</ul>
					