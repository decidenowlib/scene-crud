
<div class="container text-right">
	<span class="text-muted">&copy; {{ config('app.name') }}, {{ date('Y') }}</span>
</div>
