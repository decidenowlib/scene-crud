@extends('layouts.base.layout')

@php
	$scene = new DecideNow\Scene\Controllers\SceneBaseController()
@endphp

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('auth.verify_header') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
							Новая ссылка для подтверждения была отправлена на Ваш электронный адрес.
                        </div>
                    @endif
                    Для продолжения работы подтвердите свой e-mail по ссылке в письме, отправленном на Ваш электронный адрес.<br>
                    Если Вы не можете найти письмо со ссылкой для подтверждения, Вы можете
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">отправить письмо повторно</button>.
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
