@extends('layouts.base.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-6">
            <div class="card">
                <div class="card-header">{{ __('auth.resetpass_header') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (!$errors->isEmpty())
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        {!! DecideNowCtrl::textField('username')
							->value(old('username'))
							->state(($errors->has('username')) ? 'invalid' : '')
							->feedback(($errors->has('username')) ? $errors->first('username') : '')
							->placeholder(__('auth.username'))
							->iconBefore('fa fas fa-envelope')
							->attr('required', 'true')
							->out()
						!!}

                        {!! DecideNowCtrl::button('sendlink_button')
							->type('submit')
							->label(__('auth.sendlink_button'))
							->iconBefore('glyphicon glyphicon-ok')
							->style('outline-secondary')
							->out()
						!!}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
