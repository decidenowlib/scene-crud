@extends('layouts.base.layout')

@php
	$scene = new DecideNow\Scene\Controllers\SceneBaseController()
@endphp

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('confirm_header') }}</div>

                <div class="card-body">
                    @if (!$errors->isEmpty())
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                        </div>
                    @endif
                    
                    Для продолжения подтвердите свой пароль.

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        {!! DecideNowCtrl::textField('password')
							->type('password')
							->state(($errors->has('password')) ? 'invalid' : '')
							->feedback(($errors->has('password')) ? $errors->first('password') : '')
							->placeholder(__('auth.password'))
							->iconBefore('fas fa-key fa-fw')
							->attr('required', 'true')
							->out()
						!!}
						
						{!! DecideNowCtrl::textField('password_confirmation')
							->type('password')
							->placeholder(__('auth.password_confirmation'))
							->iconBefore('fas fa-key fa-fw')
							->attr('required', 'true')
							->out()
						!!}

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                {!! DecideNowCtrl::button('confirm_button')
                                    ->type('submit')
                                    ->label(__('auth.confirm_button'))
                                    ->iconBefore('fas fa-check fa-fw')
                                    ->style('outline-secondary')
                                    ->out()
                                !!}

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
