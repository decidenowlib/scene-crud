@extends('layouts.base.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-6">
            <div class="card">
                <div class="card-header">{{ __('auth.resetpass_header') }}</div>

                <div class="card-body">
					@if (!$errors->isEmpty())
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                        </div>
					@endif
					
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        {!! DecideNowCtrl::textField('username')
							->value(old('username'))
							->state(($errors->has('username')) ? 'invalid' : '')
							->feedback(($errors->has('username')) ? $errors->first('email') : '')
							->placeholder(__('auth.username'))
							->iconBefore('fa fa-user fa-fw')
							->attr('required', 'true')
							->attr('autofocus', 'true')
							->out()
						!!}

                        {!! DecideNowCtrl::textField('password')
							->type('password')
							->state(($errors->has('password')) ? 'invalid' : '')
							->feedback(($errors->has('password')) ? $errors->first('password') : '')
							->placeholder(__('auth.password'))
							->iconBefore('fas fa-key fa-fw')
							->attr('required', 'true')
							->out()
						!!}
						
						{!! DecideNowCtrl::textField('password_confirmation')
							->type('password')
							->placeholder(__('auth.password_confirmation'))
							->iconBefore('fas fa-key fa-fw')
							->attr('required', 'true')
							->out()
						!!}

                        {!! DecideNowCtrl::button('resetpass_button')
							->type('submit')
							->label(__('auth.resetpass_button'))
							->iconBefore('glyphicon glyphicon-ok')
							->style('outline-secondary')
							->out()
						!!}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
