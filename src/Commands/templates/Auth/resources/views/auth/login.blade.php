@extends('layouts.base.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-10 col-md-8 col-lg-6">
            <div class="card">
                <div class="card-header">{{ __('auth.login_header') }}</div>

                <div class="card-body">
					@if (!$errors->isEmpty())
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first() }}
                        </div>
					@endif
					
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
						
						{!! DecideNowCtrl::textField('username')
							->value(old('username'))
							->state(($errors->has('username')) ? 'invalid' : '')
							->feedback(($errors->has('username')) ? $errors->first('username') : '')
							->placeholder(__('auth.username'))
							->iconBefore('fas fa-user fa-fw')
							->attr('required', 'true')
							->attr('autofocus', 'true')
							->out()
						!!}
						
						{!! DecideNowCtrl::textField('password')
							->type('password')
							->state(($errors->has('password')) ? 'error' : '')
							->feedback(($errors->has('password')) ? $errors->first('password') : '')
							->placeholder(__('auth.password'))
							->iconBefore('fas fa-key fa-fw')
							->attr('required', 'true')
							->out()
						!!}
						
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								{!! DecideNowCtrl::button('login_button')
									->type('submit')
									->label(__('auth.login_button'))
									->iconBefore('fas fa-sign-in-alt fa-fw')
									->style('outline-secondary')
									->isNotGroup(false)
									->out()
								!!}
							</div>
							<div class="col-xs-12 col-sm-6 text-sm-right">
								{!! DecideNowCtrl::checkbox('remember')
									->value(1)
									->label(__('auth.remember'))
									->isChecked(old('remember'))
									->out()
								!!}
							</div>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 text-sm-right"><a class="btn btn-link" href="{{ route('register') }}">@lang('auth.register_link')</a></div>
		<div class="col-xs-12 col-sm-6"><a class="btn btn-link" href="{{ route('password.request') }}">{{ __('auth.forgotpass_link') }}</a></div>
	</div>
</div>
@endsection
