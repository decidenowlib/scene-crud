<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль изменен!',
    'sent' => 'На указанный адрес отправлена ссылка для смены пароля!',
	'throttled' => 'Дождитесь разрешения для повторной попытки',
    'token' => 'Неверный ключ восстановления пароля.',
    'user' => "Пользователь с указанным адресом не найден.",

];
