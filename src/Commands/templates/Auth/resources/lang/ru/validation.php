<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Поле :attribute должно быть подтверждено.',
    'active_url'           => 'Поле :attribute содержит не допустимый URL.',
    'after'                => 'Поле :attribute должно содержать дату после :date.',
    'after_or_equal'       => 'Поле :attribute должно содержать дату после или равную :date.',
    'alpha'                => 'Поле :attribute может содержать только буквы.',
    'alpha_dash'           => 'Поле :attribute может содержать только буквы, цифры и тире.',
    'alpha_num'            => 'Поле :attribute может содержать только буквы и цифры.',
    'array'                => 'Поле :attribute должно содержать массив.',
    'before'               => 'Поле :attribute должно содержать дату до :date.',
    'before_or_equal'      => 'Поле :attribute должно содержать дату до или равную :date.',
    'between'              => [
        'numeric' => 'Поле :attribute должно быть между :min и :max.',
        'file'    => 'Поле :attribute должно быть размером от :min до :max килобайт.',
        'string'  => 'Поле :attribute должно содержать от :min до :max символов.',
        'array'   => 'Поле :attribute должно содержать от :min до :max элементов.',
    ],
    'boolean'              => 'Поле :attribute должно быть истино или ложно.',
    'confirmed'            => 'Подтверждение поля ":attribute" отличается от значения поля.',
    'date'                 => 'Поле :attribute содержит не допустимую дату.',
	'date_equals'          => 'The :attribute must be a date equal to :date.',
    'date_format'          => 'Поле :attribute не соответствует формату :format.',
    'different'            => 'Поля :attribute и :other должны быть различны.',
    'digits'               => 'Поле :attribute должно содержать :digits разрядов.',
    'digits_between'       => 'Поле :attribute должно содержать от :min до :max разрядов.',
    'dimensions'           => 'Поле :attribute имеет неверные измерения.',
    'distinct'             => 'Поле :attribute должно быть уникально.',
    'email'                => 'Поле ":attribute" должно содержать e-mail.',
    'ends_with'            => 'The :attribute must end with one of the following: :values.',
    'exists'               => 'Выбраное значение поля :attribute не верно.',
    'file'                 => 'Поле :attribute должно содержать файл.',
    'filled'               => 'Поле :attribute должно быть заполнено.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image'                => 'Поле :attribute должно содержать изображение.',
    'in'                   => 'Выбраное значение поля :attribute не верно.',
    'in_array'             => 'Значение поля :attribute не входит в :other.',
    'integer'              => 'Поле :attribute должно содержать целое число.',
    'ip'                   => 'Поле :attribute должно содержать IP-адрес.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'Поле :attribute должно содержать JSON-строку.',
'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max'                  => [
        'numeric' => 'Поле ":attribute" должно быть не больше :max.',
        'file'    => 'Поле ":attribute" должно быть не больше :max килобайт.',
        'string'  => 'Поле ":attribute" должно быть не больше :max символов.',
        'array'   => 'Поле ":attribute" должно содержать не больше :max значений.',
    ],
    'mimes'                => 'Поле :attribute must be a file of type: :values.',
    'mimetypes'            => 'Поле :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'Поле ":attribute" должно быть не меньше :min.',
        'file'    => 'Поле ":attribute" должно быть не меньше :min килобайт.',
        'string'  => 'Поле ":attribute" должно быть не меньше :min символов.',
        'array'   => 'Поле ":attribute" должно содержать не меньше :min значений.',
    ],
    'not_in'               => 'Выбраное значение поля :attribute не верно.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric'              => 'Поле ":attribute" должно содержать число.',
    'password'             => 'The password is incorrect.',
    'present'              => 'Поле :attribute должно быть заполнено.',
    'regex'                => 'Поле :attribute имеет неверный формат.',
    'required'             => 'Поле ":attribute" обязательно для заполнения.',
    'required_if'          => 'Поле :attribute обязательно для заполнения, когда :other имеет значение :value.',
    'required_unless'      => 'Поле :attribute обязательно для заполнения, когда :other входит в :values.',
    'required_with'        => 'Поле ":attribute" обязательно для заполнения, когда заполнено одно из полей: :values.',
    'required_with_all'    => 'Поле :attribute обязательно для заполнения, когда :values заполнено.',
    'required_without'     => 'Поле :attribute обязательно для заполнения, когда :values не заполнено.',
    'required_without_all' => 'Поле :attribute обязательно для заполнения, когда не заполнено ни одно из полей :values.',
    'same'                 => 'Поля :attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => 'Поле :attribute должно быть :size.',
        'file'    => 'Поле :attribute должно быть размером :size килобайт.',
        'string'  => 'Поле :attribute должно содержать :size символов.',
        'array'   => 'Поле :attribute должно содержать :size элементов.',
    ],
    'starts_with'          => 'The :attribute must start with one of the following: :values.',
    'string'               => 'Поле ":attribute" должно содержать строку.',
    'timezone'             => 'Поле ":attribute" должно содержать часовой пояс.',
    'unique'               => 'Поле ":attribute" не уникально.',
    'uploaded'             => 'Поле ":attribute" загрузить не удалось.',
    'url'                  => 'Поле ":attribute" имеет неверный формат.',
    'uuid'                 => 'The :attribute must be a valid UUID.',
		

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
    	'name'=>'Имя',
    	'username'=>'Логин/E-mail',
    	'password'=>'Пароль',
    ],

];
