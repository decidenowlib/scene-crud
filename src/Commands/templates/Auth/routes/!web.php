Auth::routes(['verify' => true]);

Route::middleware(['auth', 'verified'])->group(function () {

    // HomeBase

    Route::get('/base', 'Home\Base\HomeBaseController@get')->name('base');
    Route::post('/base', 'Home\Base\HomeBaseController@post');

});