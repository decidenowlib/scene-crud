<?php

namespace App\Http\Controllers\Home\Base;

use DecideNow\Scene\Controllers\SceneBaseController;
use Illuminate\Http\Request;

class HomeBaseController extends SceneBaseController
{	
	public static $scene_key = 'home-base';
	
	public function prepareContent($request, $has_content = true)
	{
		$this->has_content = $has_content;
		return true;
	}
	
	public function prepareResponse($request, $has_content = true)
	{
		if ($has_content) {
			$this->prepareContent($request, $has_content);
		}
		return $this->sceneResponse();
	}
	
	public function get(Request $request)
	{
		$this->prepareGet($request);
		return $this->prepareResponse($request);
	}
	
	public function post(Request $request)
	{
		$this->preparePost($request);
		
		if ($this->task == 'some_task') {
			// do something
		}
		
		return $this->prgRedirect() ?: $this->prepareResponse($request);
	}
}