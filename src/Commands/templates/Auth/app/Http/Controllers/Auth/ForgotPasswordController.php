<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use DecideNow\Scene\Controllers\SceneBaseController;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends SceneBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    protected function credentials(Request $request)
    {
        return $request->only('username');
    }

    protected function validateEmail(Request $request)
    {
        $request->validate(['username' => 'required|email']);
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email')->with($this->sceneVariables());
    }
}
