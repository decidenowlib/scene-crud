<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use DecideNow\Scene\Controllers\SceneBaseController;
use Illuminate\Foundation\Auth\ConfirmsPasswords;

class ConfirmPasswordController extends SceneBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Confirm Password Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password confirmations and
    | uses a simple trait to include the behavior. You're free to explore
    | this trait and override any functions that require customization.
    |
    */

    use ConfirmsPasswords;

    /**
     * Where to redirect users when the intended url fails.
     *
     * @var string
     */
    protected function redirectTo() {
        return route('base');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($scene_parent = null, $scene_id = '', $scene_id_suffix = '')
    {
        parent::__construct($scene_parent, $scene_id, $scene_id_suffix);
        $this->middleware('auth');
    }

    public function showConfirmForm()
    {
        return view('auth.passwords.confirm')->with($this->sceneVariables());
    }
}
