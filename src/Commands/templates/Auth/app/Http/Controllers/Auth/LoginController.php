<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use DecideNow\Scene\Controllers\SceneBaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends SceneBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo() {
        return route('base');
    }
    
    protected function loggedOut(Request $request) {
        return redirect('base');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($scene_parent = null, $scene_id = '', $scene_id_suffix = '')
    {
        parent::__construct($scene_parent, $scene_id, $scene_id_suffix);
        $this->middleware('guest')->except('logout');
    }

    public function username() { 
        return 'username';
    }

    public function showLoginForm()
    {
        return view('auth.login')->with($this->sceneVariables());
    }
   
}
