<?php

namespace App\ORM;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class User extends ORMModelAuth implements MustVerifyEmail
{
	public $userRole;

	protected $table = 'user';
	protected $fillable = [
		'name', 'username', 'password',
	];
	
	public function __construct($attributes = [])
	{
		parent::__construct($attributes);
		$this->userRole = (object) ['code' => 'admin'];
	}

	public static function getAliases()
	{
		return [
			'table_name' => 'Пользователи', 
			'record_name' => 'Пользователь', 
			'username' => 'Логин', 
			'password' => 'Пароль', 

		] + parent::getAliases();
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'universal' => '', 'username' => '', 'email_verified_at' => '', 'password' => '', 'remember_token' => '',  ];
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'universal', '');
		if ($tmp != '') {
			$query = $query
			->orWhere('name', 'LIKE', '%'.$tmp.'%')
			->orWhere('username', 'LIKE', '%'.$tmp.'%')
			->orWhere('email_verified_at', 'LIKE', '%'.$tmp.'%')
			->orWhere('password', 'LIKE', '%'.$tmp.'%')
			->orWhere('remember_token', 'LIKE', '%'.$tmp.'%');;
		}

		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'username' => '', 'email_verified_at' => '', 'password' => '', 'remember_token' => '',  ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'username', '');
		if ($tmp != '') {
			$query = $query->orderBy('username', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'email_verified_at', '');
		if ($tmp != '') {
			$query = $query->orderBy('email_verified_at', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'password', '');
		if ($tmp != '') {
			$query = $query->orderBy('password', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'remember_token', '');
		if ($tmp != '') {
			$query = $query->orderBy('remember_token', $tmp);
		}

		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	
	

	
	/* list queries */
	
	public static function defaultListQuery($query = null)
	{
		$query = parent::defaultListQuery($query);
		//$query = $query->select('user.*')->distinct();
		return $query;
	}

	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					//return true;
					return false;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
