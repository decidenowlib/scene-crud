# Scene CRUD #

## Описание ##
Расширение для библиотеки Scene для Laravel, позволяющее создавать шаблоны CRUD.

## Установка ##
Исходный код проекта "живёт" на Bitbucket, поэтому для начала необходимо подключить репозиторий, а затем добавить пакет:

```
composer config repositories.decidenow.scene.crud vcs https://bitbucket.org/decidenowlib/scene-crud
composer require decidenow/scene-crud
```

## Автор ##
Соколов Александр

E-mail: sanya_sokolov@inbox.ru
